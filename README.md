Create project documentation on GitLab pages (Automatic deployment of documentation using gitlab CI).

**Steps:**
1. Create new project
2. Add test folder and index.html
3. Step 3. Add .gitlab-ci.yml
4. Step 4. Wait till pipeline to be finished.
5. Step 5. Go to project settings => Pages => Access pages
6. Click on the generated link.
